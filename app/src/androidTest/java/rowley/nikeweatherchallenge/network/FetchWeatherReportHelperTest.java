package rowley.nikeweatherchallenge.network;

import android.text.TextUtils;

import rowley.nikeweatherchallenge.WeatherChallengeActivityTestBase;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;

/**
 * Created by joe on 4/5/15.
 */
public class FetchWeatherReportHelperTest extends WeatherChallengeActivityTestBase {

    public void testGetWeatherReportSimple() {
        WeatherReport report = FetchWeatherReportHelper.getReportForZipCode(mActivity, 97006);
        assertNotNull(report);
        assertNotNull(report.getCurrentWeather());
        assertNotNull(report.getCurrentWeather().get(0));
        assertFalse(TextUtils.isEmpty(report.getCurrentWeather().get(0).getWeatherText()));
    }

    public void testGetWeatherReportDifferentZipCode() {
        WeatherReport reportOne = FetchWeatherReportHelper.getReportForZipCode(mActivity, 97006);
        assertNotNull(reportOne);
        WeatherReport reportTwo = FetchWeatherReportHelper.getReportForZipCode(mActivity, 83687);
        assertNotNull(reportTwo);

        assertFalse(reportOne.equals(reportTwo));
    }
}
