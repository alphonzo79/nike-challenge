package rowley.nikeweatherchallenge.data;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteStatement;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.dao.SettingsDao;

/**
 * Created by joe on 4/5/15.
 */
public class WeatherDbHelper extends SQLiteOpenHelper {
    private static final String DATABASE_NAME = "NikeWeatherChallengeDb";
    private static final int VERSION = 1;

    private final String CREATE_SETTINGS_TABLE;
    private final String CREATE_RECORDS_TABLE;

    public WeatherDbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
        CREATE_SETTINGS_TABLE = context.getString(R.string.create_settings_table);
        CREATE_RECORDS_TABLE = context.getString(R.string.create_reports_table);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.beginTransaction();

        try {
            db.execSQL(CREATE_SETTINGS_TABLE);
            db.execSQL(CREATE_RECORDS_TABLE);

            db.setTransactionSuccessful();
        } catch(SQLiteException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
        }

        populateSettingTableDefaults(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //do nothing at this time. Not upgrading yet
    }

    private void populateSettingTableDefaults(SQLiteDatabase sqLiteDatabase) {
        SQLiteStatement sqlStatement = null;

        sqLiteDatabase.beginTransaction();
        try{
            sqlStatement = sqLiteDatabase.compileStatement(String.format("INSERT INTO %s (%s, %s) VALUES (?, ?)", SettingsDao.TABLE_NAME, SettingsDao.COLUMN_SETTING_NAME, SettingsDao.COLUMN_SETTING_VALUE));
            sqlStatement.bindString(1, SettingsDao.SETTING_NAME_RECORD_LAST_UPDATED);
            sqlStatement.bindString(2, "0");

            sqlStatement.executeInsert();
            sqlStatement.clearBindings();

            sqlStatement.bindString(1, SettingsDao.SETTING_NAME_IS_METRIC);
            sqlStatement.bindString(2, Boolean.toString(true));

            sqlStatement.executeInsert();
            sqlStatement.clearBindings();

            sqlStatement.bindString(1, SettingsDao.SETTING_NAME_ZIP_CODE);
            sqlStatement.bindString(2, "97006");

            sqlStatement.executeInsert();

            sqLiteDatabase.setTransactionSuccessful();
        }
        catch (SQLException ex) {
            ex.printStackTrace();
        }
        finally {
            sqLiteDatabase.endTransaction();
            if(sqlStatement != null) {
                sqlStatement.close();
            }
        }
    }
}
