package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class WeatherReport implements Serializable, Parcelable {
    @SerializedName("curren_weather")
    private List<CurrentWeather> currentWeather;
    private List<Forecast> forecast;

    public WeatherReport() {}

    private WeatherReport(Parcel in) {
        if(in.readInt() == 1) {
            setCurrentWeather(new ArrayList<CurrentWeather>());
            in.readTypedList(getCurrentWeather(), CurrentWeather.CREATOR);
        }
        if(in.readInt() == 1) {
            setForecast(new ArrayList<Forecast>());
            in.readTypedList(getForecast(), Forecast.CREATOR);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(getCurrentWeather() != null ? 1 : 0);
        if(getCurrentWeather() != null) {
            dest.writeTypedList(getCurrentWeather());
        }
        dest.writeInt(getForecast() != null ? 1 : 0);
        if(getForecast() != null) {
            dest.writeTypedList(getForecast());
        }
    }

    public static final Parcelable.Creator<WeatherReport> CREATOR = new Creator<WeatherReport>() {
        @Override
        public WeatherReport createFromParcel(Parcel parcel) {
            return new WeatherReport(parcel);
        }

        @Override
        public WeatherReport[] newArray(int size) {
            return new WeatherReport[size];
        }
    };

    public List<CurrentWeather> getCurrentWeather() {
        return currentWeather;
    }

    public void setCurrentWeather(List<CurrentWeather> currentWeather) {
        this.currentWeather = currentWeather;
    }

    public List<Forecast> getForecast() {
        return forecast;
    }

    public void setForecast(List<Forecast> forecast) {
        this.forecast = forecast;
    }

    @Override
    public String toString() {
        return GsonProvider.getInstance().getGson().toJson(this);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(currentWeather != null && !currentWeather.isEmpty()) {
            for (CurrentWeather current : currentWeather) {
                hash = hash * 21 + current.hashCode();
            }
        }
        if(forecast != null && !forecast.isEmpty()) {
            for (Forecast forecaseObj : forecast) {
                hash = hash * 12 + forecaseObj.hashCode();
            }
        }

        return hash;
    }

    @Override
    public boolean equals(Object in) {
        boolean result = true;

        if(!(in instanceof WeatherReport)) {
            result = false;
        }

        if(result) {
            WeatherReport cast = (WeatherReport) in;

            if(currentWeather == null) {
                if (cast.currentWeather != null) {
                    result = false;
                }
            } else if(cast.currentWeather == null) {
                result = false;
            } else if(currentWeather.size() != cast.currentWeather.size()) {
                result = false;
            } else {
                for(int i = 0; i < currentWeather.size(); i++) {
                    if(!currentWeather.get(i).equals(cast.currentWeather.get(i))) {
                        result = false;
                    }
                }
            }

            if(forecast == null) {
                if(cast.forecast != null) {
                    result = false;
                }
            } else if(cast.forecast == null) {
                result = false;
            } else if(forecast.size() != cast.forecast.size()) {
                result = false;
            } else {
                for(int i = 0; i < forecast.size(); i++) {
                    if(!forecast.get(i).equals(cast.forecast.get(i))) {
                        result = false;
                    }
                }
            }
        }


        return result;
    }
}
