package rowley.nikeweatherchallenge.util;

/**
 * Created by joe on 4/4/15.
 */
public class TempUnitConstants {
    public static final String CELCIUS = "c";
    public static final String FAHRENHEIT = "f";
}
