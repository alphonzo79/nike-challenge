package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.Forecast;
import rowley.nikeweatherchallenge.data.entity.HalfDayForecast;

/**
 * Created by joe on 4/6/15.
 */
public class TomorrowDisplayView extends RelativeLayout {
    private Forecast forecast;
    private boolean isMetric;
    private int zipCode;

    private TextView dateDisplay;
    private HalfDayForcastViewParent daytimeForecast, nighttimeForecast;

    private final String DATE_STRING_FORMAT = "MMMM dd, yyyy";

    public TomorrowDisplayView(Context context) {
        super(context);
        initView(context);
    }

    public TomorrowDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TomorrowDisplayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.view_next_day_forecast, this);

        dateDisplay = (TextView)findViewById(R.id.date_display);
        daytimeForecast = (HalfDayForcastViewParent)findViewById(R.id.daytime_forecast);
        nighttimeForecast = (HalfDayForcastViewParent)findViewById(R.id.nightime_forecast);
    }

    public void setForecast(Forecast forecast, boolean isMetric, int zipCode) {
        this.forecast = forecast;
        this.isMetric = isMetric;
        this.zipCode = zipCode;

        Calendar cal = Calendar.getInstance();
        cal.roll(Calendar.DAY_OF_MONTH, true);
        String dateString = new SimpleDateFormat(DATE_STRING_FORMAT, Locale.US).format(cal.getTime());
        dateDisplay.setText(dateString);

        HalfDayForecast day = null;
        int daytimeHigh = Integer.MIN_VALUE;
        String tempUnits = "";
        if(forecast != null) {
            daytimeHigh = forecast.getDayMaxTempAsInt();
            tempUnits = forecast.getTempUnit();
            if (forecast.getDay() != null && !forecast.getDay().isEmpty()) {
                day = forecast.getDay().get(0);
            }
        }
        daytimeForecast.setData(day, daytimeHigh, tempUnits, isMetric);

        HalfDayForecast night = null;
        int nighttimeLow = Integer.MIN_VALUE;
        if(forecast != null) {
            nighttimeLow = forecast.getNightMinTempAsInt();
            if (forecast.getNight() != null && !forecast.getNight().isEmpty()) {
                night = forecast.getNight().get(0);
            }
        }
        nighttimeForecast.setData(night, nighttimeLow, tempUnits, isMetric);
    }
}
