package rowley.nikeweatherchallenge.data.entity;

/**
 * Created by joe on 4/4/15.
 */
public class CurrentWeatherTest extends WeatherEntityTestParent {

    public void testHumidityStringToInt() {
        CurrentWeather currentWeather = report.getCurrentWeather().get(0);

        int humidityInt = currentWeather.getHumidityAsInt();
        assertEquals(74, humidityInt);

        currentWeather.setHumidity("23");
        humidityInt = currentWeather.getHumidityAsInt();
        assertEquals(23, humidityInt);

        currentWeather.setHumidity("73.4");
        humidityInt = currentWeather.getHumidityAsInt();
        assertEquals(Integer.MIN_VALUE, humidityInt);

        currentWeather.setHumidity("Seventy Three");
        humidityInt = currentWeather.getHumidityAsInt();
        assertEquals(Integer.MIN_VALUE, humidityInt);
    }

    public void testPressureStringToInt() {
        CurrentWeather currentWeather = report.getCurrentWeather().get(0);

        int pressureInt = currentWeather.getPressureAsInt();
        assertEquals(1020, pressureInt);

        currentWeather.setPressure("230");
        pressureInt = currentWeather.getPressureAsInt();
        assertEquals(230, pressureInt);

        currentWeather.setPressure("173.4");
        pressureInt = currentWeather.getPressureAsInt();
        assertEquals(Integer.MIN_VALUE, pressureInt);

        currentWeather.setPressure("One Hundred Seventy Three");
        pressureInt = currentWeather.getPressureAsInt();
        assertEquals(Integer.MIN_VALUE, pressureInt);
    }

    public void testTempStringToInt() {
        CurrentWeather currentWeather = report.getCurrentWeather().get(0);

        int tempInt = currentWeather.getTempAsInt();
        assertEquals(8, tempInt);

        currentWeather.setTemp("12");
        tempInt = currentWeather.getTempAsInt();
        assertEquals(12, tempInt);

        currentWeather.setTemp("3.4");
        tempInt = currentWeather.getTempAsInt();
        assertEquals(Integer.MIN_VALUE, tempInt);

        currentWeather.setTemp("Twelve");
        tempInt = currentWeather.getTempAsInt();
        assertEquals(Integer.MIN_VALUE, tempInt);
    }
}
