package rowley.nikeweatherchallenge.activity;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.dao.ReportDao;
import rowley.nikeweatherchallenge.data.dao.SettingsDao;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.interfaces.IsMetricReporter;
import rowley.nikeweatherchallenge.interfaces.OnReceiveWeatherReportListener;
import rowley.nikeweatherchallenge.interfaces.ReportReceiverRegistrationListener;
import rowley.nikeweatherchallenge.network.FetchWeatherReportHelper;
import rowley.nikeweatherchallenge.util.GsonProvider;
import rx.Observable;
import rx.Subscriber;
import rx.android.observables.AndroidObservable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;


public class WeatherMainActivity extends ActionBarActivity implements IsMetricReporter, ReportReceiverRegistrationListener {
    private boolean isMetric = false;
    private WeatherReport report;
    private int currentZipCode;
    private long lastUpdatedTimeInMillis;

    private List<OnReceiveWeatherReportListener> reportReceivers;

    private final int UPDATE_INTERVAL_IN_MINUTES = 30;
    private final int UPDATE_INTERVAL_IN_MILLIS = UPDATE_INTERVAL_IN_MINUTES * 60 * 1000;

    private PendingIntent forceUpdateIntent;
    //We go through a process in onCreate => gatherSettingsFromTheDb => reactToLastUpdateFound to
    //schedule an alarm to force update. However, we only want to do the update if the user is
    //using the app, so we're going to cancel the alarm in onPause. Therefore, we will wan to re-instate
    //the alarm in onResume, but ONLY if we did not just go through onCreate. So, this boolean flag
    private boolean needToScheduleInOnResume = false;
    private final int FORCE_UPDATE_REQUEST_CODE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        reportReceivers = new ArrayList<OnReceiveWeatherReportListener>();
        setContentView(R.layout.activity_weather_main);

        gatherSettingsFromTheDb();
    }

    @Override
    public void onResume() {
        super.onResume();
        if(needToScheduleInOnResume) {
            //We don't know how long the user was gone. Let's get new data if we need to. Otherwise,
            //schedule an update
            long current = System.currentTimeMillis();
            if(current - lastUpdatedTimeInMillis > UPDATE_INTERVAL_IN_MILLIS) {
                fetchReportFromNetwork();
            } else {
                scheduleUpdate();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        needToScheduleInOnResume = true;
        cancelUpdate();
    }

    private void cancelUpdate() {
        if(forceUpdateIntent != null) {
            ((AlarmManager) getSystemService(Context.ALARM_SERVICE)).cancel(forceUpdateIntent);
        }
        unregisterReceiver(forceUpdateAlarmBroadcastReceiver);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_weather_main, menu);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);

        if(isMetric) {
            menu.findItem(R.id.action_set_metric).setVisible(false);
            menu.findItem(R.id.action_set_imperial).setVisible(true);
        } else {
            menu.findItem(R.id.action_set_metric).setVisible(true);
            menu.findItem(R.id.action_set_imperial).setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch(item.getItemId()) {
            case R.id.action_set_metric:
                setMetric(true);
                deliverWeatherReportToReceivers();
                return true;
            case R.id.action_set_imperial:
                setMetric(false);
                deliverWeatherReportToReceivers();
                return true;
            case R.id.action_alternate_zip:
                promptForAlternativeZip();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void setMetric(boolean isMetric) {
        this.isMetric = isMetric;
        deliverWeatherReportToReceivers();
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                new SettingsDao(WeatherMainActivity.this).setIsMetric(WeatherMainActivity.this.isMetric);
            }
        })).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).doOnCompleted(new Action0() {
            @Override
            public void call() {
                invalidateOptionsMenu();
            }
        }).subscribe();
    }

    private void deliverWeatherReportToReceivers() {
        if(reportReceivers != null && !reportReceivers.isEmpty()) {
            for(OnReceiveWeatherReportListener receiver : reportReceivers) {
                receiver.onReceiveWeatherReport(report, isMetric, currentZipCode);
            }
        }
    }

    private void promptForAlternativeZip() {
        final EditText zipInput = new EditText(this);
        zipInput.setGravity(Gravity.CENTER_HORIZONTAL);
        zipInput.setText(String.valueOf(currentZipCode));
        zipInput.setInputType(InputType.TYPE_CLASS_NUMBER);

        zipInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() > 5) {
                    s.delete(5, s.length());
                }
            }
        });
        new AlertDialog.Builder(this).setTitle(R.string.new_zip_title).setMessage(R.string.new_zip_message).
                setView(zipInput).setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        }).setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String zip = zipInput.getText().toString();
                if (zip.length() < 5) {
                    Toast.makeText(WeatherMainActivity.this, R.string.short_zip_error, Toast.LENGTH_SHORT).show();
                } else if (!TextUtils.isDigitsOnly(zip)) {
                    Toast.makeText(WeatherMainActivity.this, R.string.non_digits_error, Toast.LENGTH_SHORT).show();
                } else if (!Integer.valueOf(zip).equals(currentZipCode)) {
                    setNewZipCode(Integer.valueOf(zip));
                }
            }
        }).show();
    }

    private void setNewZipCode(int zipCode) {
        currentZipCode = zipCode;
        cancelUpdate();
        fetchReportFromNetwork();
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<Void>() {
            @Override
            public void call(Subscriber<? super Void> subscriber) {
                new SettingsDao(WeatherMainActivity.this).setZipCode(WeatherMainActivity.this.currentZipCode);
            }
        })).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    private void reactToLastUpdatedFound(long lastUpdatedTimeInMillis) {
        this.lastUpdatedTimeInMillis = lastUpdatedTimeInMillis;
        long current = System.currentTimeMillis();
        if((current - lastUpdatedTimeInMillis) > UPDATE_INTERVAL_IN_MILLIS) {
            fetchReportFromNetwork();
        } else {
            fetchLatestReportFromDb();
            scheduleUpdate();
        }
    }

    private void fetchReportFromNetwork() {
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<WeatherReport>() {
            @Override
            public void call(Subscriber<? super WeatherReport> subscriber) {
                subscriber.onNext(FetchWeatherReportHelper.getReportForZipCode(WeatherMainActivity.this, currentZipCode));
                subscriber.onCompleted();
            }
        })).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<WeatherReport>() {
            @Override
            public void call(WeatherReport result) {
                report = result;
                deliverWeatherReportToReceivers();
                if (result == null) {
                    Toast.makeText(WeatherMainActivity.this, R.string.error_fetching_report, Toast.LENGTH_SHORT).show();
                }
            }
        }).doOnCompleted(new Action0() {
            @Override
            public void call() {
                saveLatestReportToDb();
            }
        }).subscribe();
    }

    private void saveLatestReportToDb() {
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<Long>() {
            @Override
            public void call(Subscriber<? super Long> subscriber) {
                long updateTime = System.currentTimeMillis();
                new ReportDao(WeatherMainActivity.this).saveReport(updateTime, report);
                if(report != null) {
                    new SettingsDao(WeatherMainActivity.this).setReportLastUpdated(updateTime);
                }
                subscriber.onNext(updateTime);
            }
        })).doOnNext(new Action1<Long>() {
            @Override
            public void call(Long aLong) {
                lastUpdatedTimeInMillis = aLong;
                scheduleUpdate();
            }
        }).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).subscribe();
    }

    private void fetchLatestReportFromDb() {
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<WeatherReport>() {
            @Override
            public void call(Subscriber<? super WeatherReport> subscriber) {
                subscriber.onNext(new ReportDao(WeatherMainActivity.this).getLatestWeatherReport());
                subscriber.onCompleted();
            }
        })).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<WeatherReport>() {
            @Override
            public void call(WeatherReport result) {
                if (result != null) {
                    report = result;
                    deliverWeatherReportToReceivers();
                } else {
                    Toast.makeText(WeatherMainActivity.this, R.string.error_fetching_report, Toast.LENGTH_SHORT).show();
                }
            }
        }).subscribe();
    }

    private void gatherSettingsFromTheDb() {
        AndroidObservable.bindActivity(this, Observable.create(new Observable.OnSubscribe<Object>() {
            @Override
            public void call(Subscriber<? super Object> subscriber) {
                SettingsDao dao = new SettingsDao(WeatherMainActivity.this);
                subscriber.onNext(dao.getIsMetric());
                subscriber.onNext(dao.getZipCode());
                subscriber.onNext(dao.getReportLastUpdated());
                subscriber.onCompleted();
            }
        })).subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread()).doOnNext(new Action1<Object>() {
            @Override
            public void call(Object result) {
                if (result instanceof Boolean) {
                    WeatherMainActivity.this.isMetric = (Boolean) result;
                    invalidateOptionsMenu();
                } else if(result instanceof Integer) {
                    WeatherMainActivity.this.currentZipCode = (Integer)result;
                } else if (result instanceof Long) {
                    WeatherMainActivity.this.reactToLastUpdatedFound((Long) result);
                }
            }
        }).subscribe();
    }

    private void scheduleUpdate() {
        needToScheduleInOnResume = false;
        if(forceUpdateIntent == null) {
            Intent intentAlarm = new Intent();
            intentAlarm.setAction(UPDATE_ALARM_INTENT_FILTER);
            forceUpdateIntent = PendingIntent.getBroadcast(this, FORCE_UPDATE_REQUEST_CODE, intentAlarm, PendingIntent.FLAG_UPDATE_CURRENT);
        }

        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        //set the alarm for particular time
        alarmManager.set(AlarmManager.RTC, lastUpdatedTimeInMillis + UPDATE_INTERVAL_IN_MILLIS, forceUpdateIntent);
        registerReceiver(forceUpdateAlarmBroadcastReceiver, new IntentFilter(UPDATE_ALARM_INTENT_FILTER));
    }

    @Override
    public boolean getIsMetric() {
        return isMetric;
    }

    @Override
    public void registerReportReceiver(OnReceiveWeatherReportListener receiver) {
        if(reportReceivers == null) {
            reportReceivers = new ArrayList<OnReceiveWeatherReportListener>();
        }
        if(!reportReceivers.contains(receiver)) {
            reportReceivers.add(receiver);
            if(report != null) {
                receiver.onReceiveWeatherReport(report, isMetric, currentZipCode);
            }
        }
    }

    @Override
    public void removeReportReceiver(OnReceiveWeatherReportListener receiver) {
        if(reportReceivers != null) {
            reportReceivers.remove(receiver);
        }
    }

    public final static String UPDATE_ALARM_INTENT_FILTER = "rowley.weatherchallenge.UPDATE_ALARM";
    private BroadcastReceiver forceUpdateAlarmBroadcastReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            fetchReportFromNetwork();
        }
    };
}
