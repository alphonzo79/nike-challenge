package rowley.nikeweatherchallenge.data.dao;

import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import com.google.gson.JsonParseException;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.WeatherChallengeActivityTestBase;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/5/15.
 */
public class ReportDaoTest extends WeatherChallengeActivityTestBase {
    private ReportDao mDao;

    //to clean up
    private long originalId = 0;
    private WeatherReport originalReport = null;
    private long originalUpdatedTime = 0l;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mDao = new ReportDao(mActivity);

        SQLiteDatabase db = mDao.getReadableDatabase();
        Cursor cursor = db.query(ReportDao.TABLE_NAME, new String[]{ReportDao.COLUMN_ID, ReportDao.COLUMN_TIME_LOADED, ReportDao.COLUMN_JSON},
                null, null, null, null, null);
        if(cursor != null) {
            try {
                cursor.moveToFirst();
                originalId = cursor.getLong(0);
                originalUpdatedTime = cursor.getLong(1);
                originalReport = GsonProvider.getInstance().getGson().fromJson(cursor.getString(2), WeatherReport.class);
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        db.close();
    }

    @Override
    public void tearDown() throws Exception {
        writeDirectlyToDb(originalId, originalUpdatedTime, originalReport);
        super.tearDown();
    }

    private void writeDirectlyToDb(long id, long updateTime, WeatherReport report) {
        SQLiteDatabase db = mDao.getWritableDatabase();

        db.beginTransaction();
        try {
            SQLiteStatement stmt = db.compileStatement(String.format(Locale.US, "UPDATE %s SET %s=?, %s=? WHERE %s=?", ReportDao.TABLE_NAME, ReportDao.COLUMN_TIME_LOADED, ReportDao.COLUMN_JSON, ReportDao.COLUMN_ID));
            stmt.bindLong(1, updateTime);
            if(report != null) {
                stmt.bindString(2, GsonProvider.getInstance().getGson().toJson(report));
            } else {
                stmt.bindNull(2);
            }
            stmt.bindLong(3, id);

            stmt.executeUpdateDelete();
            db.setTransactionSuccessful();
            stmt.close();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            db.endTransaction();
            db.close();
        }
    }

    private WeatherReport getAssetReport() {
        WeatherReport report = null;
        try {
            InputStream input = mActivity.getResources().openRawResource(R.raw.weather_report);
            ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
            byte[] part = new byte[2048];
            int numRead;
            while ((numRead = input.read(part)) != -1) {
                byteOutput.write(part, 0, numRead);
            }
            input.close();

            String reportJson = byteOutput.toString();
            report = GsonProvider.getInstance().getGson().fromJson(reportJson, WeatherReport.class);
        } catch(IOException e) {
            e.printStackTrace();
        }

        return report;
    }

    public void testRetrieveNullAddFirstReport() {
        //set up null
        writeDirectlyToDb(originalId, 0, null);

        assertNull("The report didn't null out like we wanted", mDao.getLatestWeatherReport());

        WeatherReport report = getAssetReport();
        mDao.saveReport(System.currentTimeMillis(), report);

        assertEquals("The report didn't save as expected", report, mDao.getLatestWeatherReport());
    }

    public void testTryToSaveNullFails() {
        assertFalse(mDao.saveReport(System.currentTimeMillis(), null));
    }

    public void testUpdateReport() {
        WeatherReport original = getAssetReport();
        WeatherReport update = getAssetReport();

        assertTrue("the original save didn't work", mDao.saveReport(System.currentTimeMillis(), original));
        assertEquals("The original didn't come back to us", original, mDao.getLatestWeatherReport());

        update.getCurrentWeather().get(0).setTemp("45");
        assertTrue("the update didn't work", mDao.saveReport(System.currentTimeMillis(), update));
        assertFalse("The original was still there", original.equals(mDao.getLatestWeatherReport()));
        assertEquals("The updated report didn't save", update, mDao.getLatestWeatherReport());
    }
}
