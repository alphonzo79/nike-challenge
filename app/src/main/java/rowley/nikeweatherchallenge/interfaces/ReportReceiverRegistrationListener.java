package rowley.nikeweatherchallenge.interfaces;

/**
 * Created by joe on 4/5/15.
 */
public interface ReportReceiverRegistrationListener {
    public void registerReportReceiver(OnReceiveWeatherReportListener receiver);
    public void removeReportReceiver(OnReceiveWeatherReportListener receiver);
}
