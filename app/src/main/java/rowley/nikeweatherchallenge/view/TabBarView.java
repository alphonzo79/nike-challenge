package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import rowley.nikeweatherchallenge.R;

/**
 * Created by joe on 4/6/15.
 */
public class TabBarView extends LinearLayout implements View.OnClickListener {
    private TextView currentTab, tomorrowTab;
    private FocusOptions currentlyFocused = FocusOptions.NEITHER;

    private OnTabSelectedListener onTabSelectedListener;

    private int focusedHeight, unfocusedHeight, unFocusedTopMargin, focusedTextSize, unFocusedTextSize;

    public TabBarView(Context context) {
        super(context);
        initView(context);
    }

    public TabBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public TabBarView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.view_pager_tab_bar, this);

        currentTab = (TextView)findViewById(R.id.current_tab_indicator);
        currentTab.setOnClickListener(this);
        tomorrowTab = (TextView)findViewById(R.id.tomorrow_tab_indicator);
        tomorrowTab.setOnClickListener(this);

        focusedHeight = context.getResources().getDimensionPixelOffset(R.dimen.tab_focused_height);
        unfocusedHeight = context.getResources().getDimensionPixelOffset(R.dimen.tab_unfocused_height);
        unFocusedTopMargin = context.getResources().getDimensionPixelOffset(R.dimen.tab_unfocused_top_margin);
        focusedTextSize = 18;
        unFocusedTextSize = 16;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.current_tab_indicator:
                if(onTabSelectedListener != null) {
                    onTabSelectedListener.onTabSelected(FocusOptions.CURRENT);
                }
                break;
            case R.id.tomorrow_tab_indicator:
                if(onTabSelectedListener != null) {
                    onTabSelectedListener.onTabSelected(FocusOptions.TOMORROW);
                }
                break;
        }
    }

    private void setCurrentFocused() {
        if(currentlyFocused != FocusOptions.CURRENT) {
            currentlyFocused = FocusOptions.CURRENT;
            setTabUnselected(tomorrowTab);
            setTabSelected(currentTab);
        }
    }

    private void setTomorrowFocused() {
        if(currentlyFocused != FocusOptions.TOMORROW) {
            currentlyFocused = FocusOptions.TOMORROW;
            setTabUnselected(currentTab);
            setTabSelected(tomorrowTab);
        }
    }

    private void setTabUnselected(TextView tabToUnselect) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)tabToUnselect.getLayoutParams();
        params.height = unfocusedHeight;
        params.topMargin = unFocusedTopMargin;
        tabToUnselect.setTextSize(unFocusedTextSize);
        tabToUnselect.setTypeface(Typeface.DEFAULT);
        tabToUnselect.setBackgroundResource(R.drawable.inactive_tab_background);
    }

    private void setTabSelected(TextView tabToSelect) {
        LinearLayout.LayoutParams params = (LinearLayout.LayoutParams)tabToSelect.getLayoutParams();
        params.height = focusedHeight;
        params.topMargin = 0;
        tabToSelect.setTextSize(focusedTextSize);
        tabToSelect.setTypeface(Typeface.DEFAULT_BOLD);
        tabToSelect.setBackgroundResource(R.color.current_tab_color);
    }

    /**
     * Keep the tab indicator bar in sync with the view pager. Call this to set one tab or the other as focused.
     * @param toFocus
     */
    public void setCurrentlyFocused(FocusOptions toFocus) {
        switch(toFocus) {
            case CURRENT:
                setCurrentFocused();
                break;
            case TOMORROW:
                setTomorrowFocused();
                break;
        }
    }

    public void setOnTabSelectedListener(OnTabSelectedListener onTabSelectedListener) {
        this.onTabSelectedListener = onTabSelectedListener;
    }

    public interface OnTabSelectedListener {
        public void onTabSelected(FocusOptions selectedOption);
    }

    public enum FocusOptions {
        CURRENT, TOMORROW, NEITHER;
    }
}
