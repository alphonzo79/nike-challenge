package rowley.nikeweatherchallenge.util;

/**
 * Created by joe on 4/4/15.
 */
public class WindSpeedUnitConstants {
    public static final String METERS_PER_SECOND = "mps";
    public static final String MILES_PER_HOUR = "mph";
    public static final String KILOMETERS_PER_HOUR = "kph";
}
