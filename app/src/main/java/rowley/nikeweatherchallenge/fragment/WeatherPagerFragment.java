package rowley.nikeweatherchallenge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.CurrentWeather;
import rowley.nikeweatherchallenge.data.entity.Forecast;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.interfaces.OnReceiveWeatherReportListener;
import rowley.nikeweatherchallenge.interfaces.ReportReceiverRegistrationListener;
import rowley.nikeweatherchallenge.view.CurrentDisplayView;
import rowley.nikeweatherchallenge.view.TabBarView;
import rowley.nikeweatherchallenge.view.TomorrowDisplayView;

/**
 * Created by joe on 4/4/15.
 */
public class WeatherPagerFragment extends PopulateReportParent implements TabBarView.OnTabSelectedListener, ViewPager.OnPageChangeListener {

    private TabBarView tabBarView;
    private TabsAdapter adapter;
    private ViewPager viewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_weather_pager, container, false);

        tabBarView = (TabBarView)root.findViewById(R.id.indicator_bar);
        tabBarView.setOnTabSelectedListener(this);

        adapter = new TabsAdapter();
        viewPager = (ViewPager)root.findViewById(R.id.view_pager);
        viewPager.setAdapter(adapter);
        viewPager.setOnPageChangeListener(this);

        return root;
    }

    @Override
    public void onTabSelected(TabBarView.FocusOptions selectedOption) {
        if(selectedOption == TabBarView.FocusOptions.CURRENT) {
            viewPager.setCurrentItem(0);
        } else if(selectedOption == TabBarView.FocusOptions.TOMORROW) {
            viewPager.setCurrentItem(1);
        }
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //do nothing
    }

    @Override
    public void onPageSelected(int position) {
        if(position == 0) {
            tabBarView.setCurrentlyFocused(TabBarView.FocusOptions.CURRENT);
        } else {
            tabBarView.setCurrentlyFocused(TabBarView.FocusOptions.TOMORROW);
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
        //do nothing
    }

    @Override
    protected void populateRelevantViews() {
        setDataToCurrentDisplayView();
        setDataToTomorrowDisplayView();
    }

    private class TabsAdapter extends PagerAdapter {


        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            View result = null;

            if(position == 0) {
                if(currentDisplayView == null) {
                    currentDisplayView = new CurrentDisplayView(getActivity());
                }
                setDataToCurrentDisplayView();
                result = currentDisplayView;
            } else {
                if(tomorrowDisplayView == null) {
                    tomorrowDisplayView = new TomorrowDisplayView(getActivity());
                }
                setDataToTomorrowDisplayView();
                result = tomorrowDisplayView;
            }

            container.addView(result);

            return result;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((RelativeLayout)object);
        }
    }
}
