package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.util.AttributeSet;

import rowley.nikeweatherchallenge.R;

/**
 * Created by joe on 4/8/15.
 */
public class DaytimeHalfDayForecastView extends HalfDayForcastViewParent {
    public DaytimeHalfDayForecastView(Context context) {
        super(context);
    }

    public DaytimeHalfDayForecastView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }


    public DaytimeHalfDayForecastView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getBackgroundResource() {
        return android.R.color.transparent;
    }

    @Override
    protected String getTemperatureStringFormatted() {
        return getResources().getString(R.string.day_time_high_formatted);
    }
}
