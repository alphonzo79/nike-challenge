package rowley.nikeweatherchallenge.data.entity;

/**
 * Created by joe on 4/4/15.
 */
public class WindTest extends WeatherEntityTestParent {

    public void testWindDirectionAsInt() {
        Wind wind = report.getCurrentWeather().get(0).getWind().get(0);
        assertEquals(157, wind.getDirDegreesAsInt());

        wind = report.getForecast().get(0).getDay().get(0).getWind().get(0);
        assertEquals(191, wind.getDirDegreesAsInt());
        wind.setDirDegrees(null);
        assertEquals(180, wind.getDirDegreesAsInt());
    }

    public void testWindSpeedAsInt() {
        Wind wind = report.getCurrentWeather().get(0).getWind().get(0);
        assertEquals(3, wind.getSpeedAsInt());

        wind.setSpeed("12");
        assertEquals(12, wind.getSpeedAsInt());

        wind.setSpeed("12.5");
        assertEquals(Integer.MIN_VALUE, wind.getSpeedAsInt());

        wind.setSpeed("Twelve");
        assertEquals(Integer.MIN_VALUE, wind.getSpeedAsInt());
    }
}
