package rowley.nikeweatherchallenge.util;

/**
 * Created by joe on 4/4/15.
 */
public class MetricConverter {

    //F = (C x (9 / 5)) + 32
    //(F - 32) * (5 / 9) = C

    /**
     * Return the metric equivalent of the input temperature IF the input unit is {@link rowley.nikeweatherchallenge.util.TempUnitConstants#FAHRENHEIT}. If not, then it will simply return the input. The returned value is only an approximation since it is the int value (round down)
     * @param inputValue
     * @param inputUnits
     * @return
     */
    public static int getTempInMetric(int inputValue, String inputUnits) {
        int result = inputValue;

        if(TempUnitConstants.FAHRENHEIT.equals(inputUnits)) {
            result = ((inputValue - 32) * 5) / 9;
        }

        return result;
    }

    /**
     * Return the imperial equivalent of the input temperature IF the input unit is {@link rowley.nikeweatherchallenge.util.TempUnitConstants#CELCIUS}. If not, then it will simply return the input. The returned value is only an approximation since it is the int value (round down)
     * @param inputValue
     * @param inputUnits
     * @return
     */
    public static int getTempInImperial(int inputValue, String inputUnits) {
        int result = inputValue;

        if(TempUnitConstants.CELCIUS.equals(inputUnits)) {
            result = ((inputValue * 9) / 5) + 32;
        }

        return result;
    }


    //K = M / 0.62137;
    //M = K * 0.62137;

    /**
     * Figure the metric value (KPH) of the provided wind speed IF the input unit is {@link rowley.nikeweatherchallenge.util.WindSpeedUnitConstants#MILES_PER_HOUR}. If not, then it will simply return the input. The returned value is only an approximation since it is an int value. Also, if the input unit is {@link rowley.nikeweatherchallenge.util.WindSpeedUnitConstants#METERS_PER_SECOND} it will be converted into KPH
     * @param inputValue
     * @param inputUnit
     * @return
     */
    public static int getWindSpeedInMetric(int inputValue, String inputUnit) {
        int result = inputValue;

        if(WindSpeedUnitConstants.MILES_PER_HOUR.equals(inputUnit)) {
            result = (int)(inputValue / 0.62137);
        } else if(WindSpeedUnitConstants.METERS_PER_SECOND.equals(inputUnit)) {
            //1000 meters per KM, 3600 seconds per hour
            result = (inputValue * 3600) / 1000;
        }

        return result;
    }

    /**
     * Figure the imperial value (MPH) of the provided wind speed IF the input unit is {@link rowley.nikeweatherchallenge.util.WindSpeedUnitConstants#KILOMETERS_PER_HOUR} or {@link rowley.nikeweatherchallenge.util.WindSpeedUnitConstants#METERS_PER_SECOND}. If not, then it will simply return the input. The returned value is only an approximation since it is an int value
     * @param inputValue
     * @param inputUnit
     * @return
     */
    public static int getWindSpeedInImperial(int inputValue, String inputUnit) {
        int result = inputValue;

        if(WindSpeedUnitConstants.KILOMETERS_PER_HOUR.equals(inputUnit)) {
            result = (int)(inputValue * 0.62137);
        } else if(WindSpeedUnitConstants.METERS_PER_SECOND.equals(inputUnit)) {
            result = (int)((inputValue * 3600 * 0.62137) / 1000);
        }

        return result;
    }
}
