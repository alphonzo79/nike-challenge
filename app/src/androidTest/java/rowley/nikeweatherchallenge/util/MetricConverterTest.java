package rowley.nikeweatherchallenge.util;

import android.test.InstrumentationTestCase;

/**
 * Created by joe on 4/4/15.
 */
public class MetricConverterTest extends InstrumentationTestCase {

    public void testFahrenheitToCelcius() {
        assertEquals(11, MetricConverter.getTempInMetric(53, TempUnitConstants.FAHRENHEIT));
        assertEquals(67, MetricConverter.getTempInMetric(153, TempUnitConstants.FAHRENHEIT));
        assertEquals(0, MetricConverter.getTempInMetric(32, TempUnitConstants.FAHRENHEIT));
        assertEquals(-40, MetricConverter.getTempInMetric(-40, TempUnitConstants.FAHRENHEIT));
        assertEquals(-95, MetricConverter.getTempInMetric(-140, TempUnitConstants.FAHRENHEIT));
        assertEquals(53, MetricConverter.getTempInMetric(53, TempUnitConstants.CELCIUS));
    }

    public void testCelciusToFahrenheit() {
        assertEquals(53, MetricConverter.getTempInImperial(12, TempUnitConstants.CELCIUS));
        assertEquals(248, MetricConverter.getTempInImperial(120, TempUnitConstants.CELCIUS));
        assertEquals(32, MetricConverter.getTempInImperial(0, TempUnitConstants.CELCIUS));
        assertEquals(-40, MetricConverter.getTempInImperial(-40, TempUnitConstants.CELCIUS));
        assertEquals(-220, MetricConverter.getTempInImperial(-140, TempUnitConstants.CELCIUS));
        assertEquals(12, MetricConverter.getTempInImperial(12, TempUnitConstants.FAHRENHEIT));
    }

    public void testMetersPerSecondToKPH() {
        assertEquals(43, MetricConverter.getWindSpeedInMetric(12, WindSpeedUnitConstants.METERS_PER_SECOND));
        assertEquals(3, MetricConverter.getWindSpeedInMetric(1, WindSpeedUnitConstants.METERS_PER_SECOND));
        assertEquals(12, MetricConverter.getWindSpeedInMetric(12, WindSpeedUnitConstants.KILOMETERS_PER_HOUR));
    }

    public void testMilesPerHourToKPH() {
        assertEquals(19, MetricConverter.getWindSpeedInMetric(12, WindSpeedUnitConstants.MILES_PER_HOUR));
        assertEquals(1, MetricConverter.getWindSpeedInMetric(1, WindSpeedUnitConstants.MILES_PER_HOUR));
        assertEquals(160, MetricConverter.getWindSpeedInMetric(100, WindSpeedUnitConstants.MILES_PER_HOUR));
        assertEquals(12, MetricConverter.getWindSpeedInMetric(12, WindSpeedUnitConstants.KILOMETERS_PER_HOUR));
    }

    public void testMetersPerSecondToMPH() {
        assertEquals(26, MetricConverter.getWindSpeedInImperial(12, WindSpeedUnitConstants.METERS_PER_SECOND));
        assertEquals(2, MetricConverter.getWindSpeedInImperial(1, WindSpeedUnitConstants.METERS_PER_SECOND));
        assertEquals(12, MetricConverter.getWindSpeedInImperial(12, WindSpeedUnitConstants.MILES_PER_HOUR));
    }

    public void testKilometersPerHourToMPH() {
        assertEquals(7, MetricConverter.getWindSpeedInImperial(12, WindSpeedUnitConstants.KILOMETERS_PER_HOUR));
        assertEquals(0, MetricConverter.getWindSpeedInImperial(1, WindSpeedUnitConstants.KILOMETERS_PER_HOUR));
        assertEquals(62, MetricConverter.getWindSpeedInImperial(100, WindSpeedUnitConstants.KILOMETERS_PER_HOUR));
        assertEquals(12, MetricConverter.getWindSpeedInImperial(12, WindSpeedUnitConstants.MILES_PER_HOUR));
    }
}
