package rowley.nikeweatherchallenge.data.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import java.util.Locale;

import rowley.nikeweatherchallenge.data.WeatherDbHelper;

/**
 * Created by joe on 4/5/15.
 */
public class SettingsDao extends WeatherDbHelper {
    public static final String TABLE_NAME = "settings";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_SETTING_NAME = "setting_name";
    public static final String COLUMN_SETTING_VALUE = "setting_value";

    public static final String SETTING_NAME_RECORD_LAST_UPDATED = "record_last_updated";
    public static final String SETTING_NAME_IS_METRIC = "is_metric";
    public static final String SETTING_NAME_ZIP_CODE = "zip_code";

    public SettingsDao(Context context) {
        super(context);
    }

    public boolean setReportLastUpdated(long updatedTimeMillis) {
        boolean success = false;

        SQLiteDatabase db = getWritableDatabase();

        SQLiteStatement stmt = db.compileStatement(String.format(Locale.US, "UPDATE %s SET %s=? WHERE %s=?", TABLE_NAME, COLUMN_SETTING_VALUE, COLUMN_SETTING_NAME));
        stmt.bindString(1, String.valueOf(updatedTimeMillis));
        stmt.bindString(2, SETTING_NAME_RECORD_LAST_UPDATED);

        db.beginTransaction();
        try {
            stmt.executeUpdateDelete();
            db.setTransactionSuccessful();
            success = true;
        } catch(SQLiteException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            db.endTransaction();
            db.close();
        }

        return success;
    }

    public long getReportLastUpdated() {
        long result = 0l;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_SETTING_VALUE}, String.format(Locale.US, "%s=?", COLUMN_SETTING_NAME),
                new String[]{SETTING_NAME_RECORD_LAST_UPDATED}, null, null, null);
        if(cursor != null) {
            try {
                cursor.moveToFirst();
                result = Long.valueOf(cursor.getString(0));
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        db.close();

        return result;
    }

    public boolean setIsMetric(boolean isMetric) {
        boolean success = false;

        SQLiteDatabase db = getWritableDatabase();

        SQLiteStatement stmt = db.compileStatement(String.format(Locale.US, "UPDATE %s SET %s=? WHERE %s=?", TABLE_NAME, COLUMN_SETTING_VALUE, COLUMN_SETTING_NAME));
        stmt.bindString(1, Boolean.toString(isMetric));
        stmt.bindString(2, SETTING_NAME_IS_METRIC);

        db.beginTransaction();
        try {
            stmt.executeUpdateDelete();
            db.setTransactionSuccessful();
            success = true;
        } catch(SQLiteException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            db.endTransaction();
            db.close();
        }

        return success;
    }

    public boolean getIsMetric() {
        boolean result = false;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_SETTING_VALUE}, String.format(Locale.US, "%s=?", COLUMN_SETTING_NAME),
                new String[]{SETTING_NAME_IS_METRIC}, null, null, null);
        if(cursor != null) {
            try {
                cursor.moveToFirst();
                result = Boolean.valueOf(cursor.getString(0));
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        db.close();

        return result;
    }

    public boolean setZipCode(int zipCode) {
        boolean success = false;

        SQLiteDatabase db = getWritableDatabase();

        SQLiteStatement stmt = db.compileStatement(String.format(Locale.US, "UPDATE %s SET %s=? WHERE %s=?", TABLE_NAME, COLUMN_SETTING_VALUE, COLUMN_SETTING_NAME));
        stmt.bindString(1, String.valueOf(zipCode));
        stmt.bindString(2, SETTING_NAME_ZIP_CODE);

        db.beginTransaction();
        try {
            stmt.executeUpdateDelete();
            db.setTransactionSuccessful();
            success = true;
        } catch(SQLiteException e) {
            e.printStackTrace();
        } finally {
            stmt.close();
            db.endTransaction();
            db.close();
        }

        return success;
    }

    public int getZipCode() {
        int result = 0;

        SQLiteDatabase db = getReadableDatabase();
        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_SETTING_VALUE}, String.format(Locale.US, "%s=?", COLUMN_SETTING_NAME),
                new String[]{SETTING_NAME_ZIP_CODE}, null, null, null);
        if(cursor != null) {
            try {
                cursor.moveToFirst();
                result = Integer.valueOf(cursor.getString(0));
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        db.close();

        return result;
    }
}
