package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class HalfDayForecast implements Serializable, Parcelable {
    @SerializedName("weather_code")
    private String weatherCode;
    @SerializedName("weather_text")
    private String weatherText;
    private List<Wind> wind;

    public HalfDayForecast() {}

    private HalfDayForecast(Parcel in) {
        if(in.readInt() == 1) {
            setWeatherCode(in.readString());
        }
        if(in.readInt() == 1) {
            setWeatherText(in.readString());
        }
        if(in.readInt() == 1) {
            setWind(new ArrayList<Wind>());
            in.readTypedList(getWind(), Wind.CREATOR);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(!TextUtils.isEmpty(getWeatherCode()) ? 1 : 0);
        if(!TextUtils.isEmpty(getWeatherCode())) {
            dest.writeString(getWeatherCode());
        }
        dest.writeInt(!TextUtils.isEmpty(getWeatherText()) ? 1 : 0);
        if(!TextUtils.isEmpty(getWeatherText())) {
            dest.writeString(getWeatherText());
        }
        dest.writeInt(getWind() != null ? 1 : 0);
        if(getWind() != null) {
            dest.writeTypedList(getWind());
        }
    }

    public static final Parcelable.Creator<HalfDayForecast> CREATOR = new Creator<HalfDayForecast>() {
        @Override
        public HalfDayForecast createFromParcel(Parcel parcel) {
            return new HalfDayForecast(parcel);
        }

        @Override
        public HalfDayForecast[] newArray(int size) {
            return new HalfDayForecast[size];
        }
    };

    public String getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(String weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public List<Wind> getWind() {
        return wind;
    }

    public void setWind(List<Wind> wind) {
        this.wind = wind;
    }

    @Override
    public String toString() {
        return GsonProvider.getInstance().getGson().toJson(this);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(!TextUtils.isEmpty(weatherCode)) {
            hash = hash * 4 + weatherCode.hashCode();
        }
        if(!TextUtils.isEmpty(weatherText)) {
            hash = hash * 6 + weatherText.hashCode();
        }
        if(wind != null && !wind.isEmpty()) {
            for (Wind windObj : wind) {
                hash = hash * 21 + wind.hashCode();
            }
        }

        return hash;
    }

    @Override
    public boolean equals(Object in) {
        boolean result = true;

        if(!(in instanceof HalfDayForecast)) {
            result = false;
        }

        if(result) {
            HalfDayForecast cast = (HalfDayForecast) in;

            if(TextUtils.isEmpty(weatherCode)) {
                if(!TextUtils.isEmpty(cast.weatherCode)) {
                    result = false;
                }
            } else if(!weatherCode.equals(cast.weatherCode)) {
                result = false;
            }

            if(TextUtils.isEmpty(weatherText)) {
                if(!TextUtils.isEmpty(cast.weatherText)) {
                    result = false;
                }
            } else if(!weatherText.equals(cast.weatherText)) {
                result = false;
            }

            if(wind == null) {
                if(cast.wind != null) {
                    result = false;
                }
            } else if(cast.wind == null) {
                result = false;
            } else if(wind.size() != cast.wind.size()) {
                result = false;
            } else {
                for(int i = 0; i < wind.size(); i++) {
                    if(!wind.get(i).equals(cast.wind.get(i))) {
                        result = false;
                    }
                }
            }
        }


        return result;
    }
}
