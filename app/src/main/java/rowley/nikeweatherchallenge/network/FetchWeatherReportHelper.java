package rowley.nikeweatherchallenge.network;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;

import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;
import java.util.Locale;

import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/5/15.
 */
public class FetchWeatherReportHelper {
    public static final String API_URL_BASE_PATTERN  = " http://www.myweather2.com/developer/forecast.ashx?uac=%s&output=json&query=%d";
//    public static String uac = "KDrRbvwbAt"; //Nike's UAC
    public static String uac = "CYiakGRSz-"; //My UAC

    public static  WeatherReport getReportForZipCode(Context context, int zipCode) {
        WeatherReport result = null;

        JsonObject json = sendRequestAndWait(zipCode, context);
        if(json != null) {
            try {
                result = GsonProvider.getInstance().getGson().fromJson(json.get("weather"), WeatherReport.class);
            } catch (JsonParseException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    private static JsonObject sendRequestAndWait(int zipCode, Context context) {
        String resultString = null;
        JsonObject result = null;

        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        if(activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting()) {
            String url = String.format(Locale.US, API_URL_BASE_PATTERN, uac, zipCode);

            OkHttpClient client = new OkHttpClient();
            Request.Builder builder = new Request.Builder().url(url);
            Request request = builder.build();

            try {
                Response response = client.newCall(request).execute();

                resultString = response.body().string();

                if(!TextUtils.isEmpty(resultString)) {
                    JsonParser parser = new JsonParser();
                    result = parser.parse(resultString).getAsJsonObject();
                }
            } catch(IOException e) {
                e.printStackTrace();
            } catch(JsonSyntaxException e) {
                e.printStackTrace();
            }
        }

        return result;
    }
}
