package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.HalfDayForecast;
import rowley.nikeweatherchallenge.data.entity.Wind;
import rowley.nikeweatherchallenge.util.MetricConverter;

/**
 * Created by joe on 4/8/15.
 */
public abstract class HalfDayForcastViewParent extends RelativeLayout {
    private TextView temperature, weatherText;
    private WindDisplayView windView;

    public HalfDayForcastViewParent(Context context) {
        super(context);
        initView(context);
    }

    public HalfDayForcastViewParent(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public HalfDayForcastViewParent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.view_half_day_forecast, this);

        temperature = (TextView)findViewById(R.id.temp_display);
        weatherText = (TextView)findViewById(R.id.weather_text);
        windView = (WindDisplayView)findViewById(R.id.wind_view);

        findViewById(R.id.root).setBackgroundResource(getBackgroundResource());
    }

    public void setData(HalfDayForecast forecast, int temperatureVal, String tempUnit, boolean isMetric) {
        if(forecast != null) {
            String tempString = "";
            if(temperatureVal != Integer.MIN_VALUE) {
                String tempUnitsString = "";
                if(isMetric) {
                    tempUnitsString = getResources().getString(R.string.celcius_unit_backup);
                    temperatureVal = MetricConverter.getTempInMetric(temperatureVal, tempUnit);
                } else {
                    tempUnitsString = getResources().getString(R.string.fahrenheit_unit_backup);
                    temperatureVal = MetricConverter.getTempInImperial(temperatureVal, tempUnit);
                }
                tempString = String.format(Locale.US, getTemperatureStringFormatted(), temperatureVal, tempUnitsString);
            } else {
                tempString = getResources().getString(R.string.n_a);
            }
            temperature.setText(tempString);

            String weather = "";
            if(!TextUtils.isEmpty(forecast.getWeatherText())) {
                weather = forecast.getWeatherText();
            } else {
                weather = getResources().getString(R.string.n_a);
            }
            weatherText.setText(weather);

            Wind wind = null;
            if(forecast.getWind() != null && !forecast.getWind().isEmpty()) {
                wind = forecast.getWind().get(0);
            }
            windView.setData(wind, isMetric);

            weatherText.setVisibility(VISIBLE);
            windView.setVisibility(VISIBLE);
        } else {
            temperature.setText(R.string.no_data_message);
            weatherText.setVisibility(GONE);
            windView.setVisibility(GONE);
        }
    }

    protected abstract int getBackgroundResource();
    protected abstract String getTemperatureStringFormatted();
}
