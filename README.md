Joe Rowley -- Nike Weather App Coding Challenge
===============================================

Thank you for the opportunity to submit my coding challenge for consideration of a position at Nike. I'm excited to learn more about your projects and needs and to see if I may have something to offer to the company. I hope you like this little app that I have put together for you.

Why Did Joe Do That? -- Decisions Made Along the Way
----------------------------------------------------

There are generally more than one way to do things, and I think it's worth while to give you a little insight into my thought process in some of my decisions along the way. Hopefully you agree with my reasoning, but the great thing about development and engineering is that sometimes we can disagree and probably learn something from each other in the process.

* <b>Why not use the Tabbed Activity template offered by Android Studio?</b>
    * Primarily it was because once you make that decision you are more or less committed to a ViewPager for all app configurations. Keeping the Activity very lean and instead letting the layout resource files react to the configuration opens us up to a future decision to use something like side-by-side fragments for a large landscape layout
    * Also, I would have immediately modified it to not use fragments anyway. Not that I have anything against fragments, but once I decided to put the pager inside of a fragment, dealing with child fragments becomes something I didn't want to mess with. Also, the data and state that we're working with is very simple and hardly worth using another layer of fragments to manage it.
* <b>Why bother with the keystore?</b>
    * Because Gradle makes configuring the build so easy I couldn't resist. I know it's not a big deal, but it's how I manage my personal projects as well as projects at work. So why not keep in the practice?

But You Did Some Stuff That Was Not In The Requirements. Shouldn't a Good Developer Stick To The Requirements?
--------------------------------------------------------------------------------------------------------------

Yes, if this were a story in a sprint assigned by a PO or a PDM then I would have stuck more strictly to the requirements. However, this is meant to demonstrate a certain ability as a developer. So I thought it acceptable to go beyond the requirements.

Additional features include:

* Metric/Imperial setting and conversion
* Ability to set an alternative ZIP code to the one that is automatically loaded in the beginning
* Alternative (non-tabbed) builds for large and extra-large devices. This instead leverages fragments either stacked (in portrait) or set side-by-side (in landscape).

I hope you like what you see. Thank you for the consideration and the opportunity
---------------------------------------------------------------------------------

<br />
<br />
