package rowley.nikeweatherchallenge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.CurrentWeather;
import rowley.nikeweatherchallenge.data.entity.Forecast;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.interfaces.OnReceiveWeatherReportListener;
import rowley.nikeweatherchallenge.interfaces.ReportReceiverRegistrationListener;
import rowley.nikeweatherchallenge.view.CurrentDisplayView;
import rowley.nikeweatherchallenge.view.TabBarView;
import rowley.nikeweatherchallenge.view.TomorrowDisplayView;

/**
 * Created by joe on 4/9/15.
 */
public class CurrentDayFragment extends PopulateReportParent implements OnReceiveWeatherReportListener {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_current_day, container, false);

        currentDisplayView = (CurrentDisplayView)root.findViewById(R.id.current_day_view);

        populateRelevantViews();

        return root;
    }

    @Override
    protected void populateRelevantViews() {
        setDataToCurrentDisplayView();
    }
}
