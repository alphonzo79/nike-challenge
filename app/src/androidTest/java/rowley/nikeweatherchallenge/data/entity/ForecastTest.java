package rowley.nikeweatherchallenge.data.entity;

import java.util.Calendar;

/**
 * Created by joe on 4/4/15.
 */
public class ForecastTest extends WeatherEntityTestParent {

    public void testDateAsCal() {
        Forecast forecastOne = report.getForecast().get(0);

        Calendar oracle = Calendar.getInstance();
        oracle.set(2015, Calendar.APRIL, 4);

        Calendar calOne = forecastOne.getDateAsCal();
        assertEquals(oracle.get(Calendar.YEAR), calOne.get(Calendar.YEAR));
        assertEquals(oracle.get(Calendar.MONTH), calOne.get(Calendar.MONTH));
        assertEquals(oracle.get(Calendar.DAY_OF_MONTH), calOne.get(Calendar.DAY_OF_MONTH));

        Forecast forecastTwo = report.getForecast().get(1);
        oracle.set(2015, Calendar.APRIL, 5);

        Calendar calTwo = forecastTwo.getDateAsCal();
        assertEquals(oracle.get(Calendar.YEAR), calTwo.get(Calendar.YEAR));
        assertEquals(oracle.get(Calendar.MONTH), calTwo.get(Calendar.MONTH));
        assertEquals(oracle.get(Calendar.DAY_OF_MONTH), calTwo.get(Calendar.DAY_OF_MONTH));

        forecastTwo.setDate("2017-09-13");
        oracle.set(2017 , Calendar.SEPTEMBER, 13);

        calTwo = forecastTwo.getDateAsCal();
        assertEquals(oracle.get(Calendar.YEAR), calTwo.get(Calendar.YEAR));
        assertEquals(oracle.get(Calendar.MONTH), calTwo.get(Calendar.MONTH));
        assertEquals(oracle.get(Calendar.DAY_OF_MONTH), calTwo.get(Calendar.DAY_OF_MONTH));

        forecastTwo.setDate("Gobbledy-Gook");
        oracle = Calendar.getInstance();

        calTwo = forecastTwo.getDateAsCal();
        assertEquals(oracle.get(Calendar.YEAR), calTwo.get(Calendar.YEAR));
        assertEquals(oracle.get(Calendar.MONTH), calTwo.get(Calendar.MONTH));
        assertEquals(oracle.get(Calendar.DAY_OF_MONTH), calTwo.get(Calendar.DAY_OF_MONTH));
    }

    public void testDayMaxTempStringToInt() {
        Forecast forecast = report.getForecast().get(0);

        int dayMax = forecast.getDayMaxTempAsInt();
        assertEquals(13, dayMax);

        forecast.setDayMaxTemp("7");
        dayMax = forecast.getDayMaxTempAsInt();
        assertEquals(7, dayMax);

        forecast.setDayMaxTemp("3.4");
        dayMax = forecast.getDayMaxTempAsInt();
        assertEquals(Integer.MIN_VALUE, dayMax);

        forecast.setDayMaxTemp("Three");
        dayMax = forecast.getDayMaxTempAsInt();
        assertEquals(Integer.MIN_VALUE, dayMax);
    }

    public void testNightMinTempStringToInt() {
        Forecast forecast = report.getForecast().get(0);

        int nightMin = forecast.getNightMinTempAsInt();
        assertEquals(5, nightMin);

        forecast.setNightMinTemp("2");
        nightMin = forecast.getNightMinTempAsInt();
        assertEquals(2, nightMin);

        forecast.setNightMinTemp("3.4");
        nightMin = forecast.getNightMinTempAsInt();
        assertEquals(Integer.MIN_VALUE, nightMin);

        forecast.setNightMinTemp("One");
        nightMin = forecast.getNightMinTempAsInt();
        assertEquals(Integer.MIN_VALUE, nightMin);
    }
}
