package rowley.nikeweatherchallenge.util;

import com.google.gson.Gson;

/**
 * Created by joe on 4/4/15.
 */
public class GsonProvider {
    private Gson gson;
    private static GsonProvider instance;
    private static Object syncLock = new Object();

    private GsonProvider() {
        gson = new Gson();
    }

    public static GsonProvider getInstance() {
        if(instance == null) {
            synchronized (syncLock) {
                if(instance == null) {
                    instance = new GsonProvider();
                }
            }
        }

        return instance;
    }

    public Gson getGson() {
        return gson;
    }
}
