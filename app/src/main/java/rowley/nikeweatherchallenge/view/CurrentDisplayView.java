package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.CurrentWeather;
import rowley.nikeweatherchallenge.data.entity.Forecast;
import rowley.nikeweatherchallenge.data.entity.HalfDayForecast;
import rowley.nikeweatherchallenge.data.entity.Wind;
import rowley.nikeweatherchallenge.util.MetricConverter;

/**
 * Created by joe on 4/6/15.
 */
public class CurrentDisplayView extends RelativeLayout {
    private CurrentWeather currentWeather;
    private boolean isMetric;
    private int zipCode;

    private TextView zipCodeDisplay, temperatureDisplay, weatherTextDisplay, humidityDisplay, barometerDisplay;
    private View emptyView, headerLabel, topDivider, dataContainer, bottomDivider, halfDaysContainer;
    private WindDisplayView windView;
    private HalfDayForcastViewParent daytimeForecast, nightimeForecast;

    public CurrentDisplayView(Context context) {
        super(context);
        initView(context);
    }

    public CurrentDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public CurrentDisplayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    public void initView(Context context) {
        inflate(context, R.layout.view_current_weather_display, this);

        zipCodeDisplay = (TextView)findViewById(R.id.zip_code_display);
        emptyView = findViewById(R.id.empty_message);
        headerLabel = findViewById(R.id.current_conditions_display);
        topDivider = findViewById(R.id.top_divider);
        dataContainer = findViewById(R.id.data_display);
        temperatureDisplay = (TextView)findViewById(R.id.temp_display);
        weatherTextDisplay = (TextView)findViewById(R.id.weather_text);
        humidityDisplay = (TextView)findViewById(R.id.humidity_report);
        barometerDisplay = (TextView) findViewById(R.id.barometer_report);
        windView = (WindDisplayView)findViewById(R.id.wind_view);
        bottomDivider = findViewById(R.id.bottom_divider);
        halfDaysContainer = findViewById(R.id.half_days_container);
        daytimeForecast = (HalfDayForcastViewParent)findViewById(R.id.daytime_forecast);
        nightimeForecast = (HalfDayForcastViewParent)findViewById(R.id.nightime_forecast);
    }

    public void setCurrentWeather(CurrentWeather currentWeather, Forecast forecast, boolean isMetric, int zipCode) {
        this.currentWeather = currentWeather;
        this.isMetric = isMetric;
        this.zipCode = zipCode;

        zipCodeDisplay.setText(String.format(Locale.US, getContext().getString(R.string.zip_code_label_formatted), zipCode));

        if(currentWeather != null) {
            emptyView.setVisibility(GONE);
            temperatureDisplay.setVisibility(VISIBLE);
            windView.setVisibility(VISIBLE);
            weatherTextDisplay.setVisibility(VISIBLE);
            barometerDisplay.setVisibility(VISIBLE);
            humidityDisplay.setVisibility(VISIBLE);

            String na = getResources().getString(R.string.n_a);

            int temperature = currentWeather.getTempAsInt();
            String tempDisplayString = na;
            if(temperature != Integer.MIN_VALUE) {
                String tempUnits = currentWeather.getTempUnit();
                if(isMetric) {
                    temperature = MetricConverter.getTempInMetric(temperature, tempUnits);
                    tempUnits = getResources().getString(R.string.celcius_unit_backup);
                } else {
                    temperature = MetricConverter.getTempInImperial(temperature, tempUnits);
                    tempUnits = getResources().getString(R.string.fahrenheit_unit_backup);
                }
                tempDisplayString = String.format(Locale.US, getResources().getString(R.string.temperature_formatted), temperature, tempUnits);
            }
            temperatureDisplay.setText(tempDisplayString);

            Wind wind = null;
            if(currentWeather.getWind() != null && !currentWeather.getWind().isEmpty()) {
                wind = currentWeather.getWind().get(0);
            }
            windView.setData(wind, isMetric);

            String weatherTextString = "";
            if(!TextUtils.isEmpty(currentWeather.getWeatherText())) {
                weatherTextString = currentWeather.getWeatherText();
            }
            weatherTextDisplay.setText(weatherTextString);

            String humidityString = na;
            if(!TextUtils.isEmpty(currentWeather.getHumidity())) {
                humidityString = String.format(Locale.US, getResources().getString(R.string.humidity_formatted), currentWeather.getHumidity());
            }
            humidityDisplay.setText(humidityString);

            String barometerString = na;
            if(!TextUtils.isEmpty(currentWeather.getPressure())) {
                barometerString = String.format(Locale.US, getResources().getString(R.string.barometer_formatted), currentWeather.getPressureAsInt());
            }
            barometerDisplay.setText(barometerString);

            if(forecast != null) {
                bottomDivider.setVisibility(VISIBLE);
                halfDaysContainer.setVisibility(VISIBLE);

                HalfDayForecast day = null;
                String tempUnits = "";
                tempUnits = forecast.getTempUnit();
                if (forecast.getDay() != null && !forecast.getDay().isEmpty()) {
                    day = forecast.getDay().get(0);
                }
                daytimeForecast.setData(day, forecast.getDayMaxTempAsInt(), tempUnits, isMetric);

                HalfDayForecast night = null;
                if (forecast.getNight() != null && !forecast.getNight().isEmpty()) {
                    night = forecast.getNight().get(0);
                }
                nightimeForecast.setData(night, forecast.getNightMinTempAsInt(), tempUnits, isMetric);
            } else {
                bottomDivider.setVisibility(GONE);
                halfDaysContainer.setVisibility(GONE);
            }
        } else {
            emptyView.setVisibility(VISIBLE);
            temperatureDisplay.setVisibility(GONE);
            windView.setVisibility(GONE);
            weatherTextDisplay.setVisibility(GONE);
            barometerDisplay.setVisibility(GONE);
            humidityDisplay.setVisibility(GONE);
            bottomDivider.setVisibility(GONE);
            halfDaysContainer.setVisibility(GONE);
        }
    }
}
