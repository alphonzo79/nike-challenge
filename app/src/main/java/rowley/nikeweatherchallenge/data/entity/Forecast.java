package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class Forecast implements Serializable, Parcelable {
    private String date;
    private List<HalfDayForecast> day;
    @SerializedName("day_max_temp")
    private String dayMaxTemp;
    private List<HalfDayForecast> night;
    @SerializedName("night_min_temp")
    private String nightMinTemp;
    @SerializedName("temp_unit")
    private String tempUnit;

    public static final transient String DATE_FORMAT = "yyyy-MM-dd";

    public Forecast() {}

    private Forecast(Parcel in) {
        if(in.readInt() == 1) {
            setDate(in.readString());
        }
        if(in.readInt() == 1) {
            setDay(new ArrayList<HalfDayForecast>());
            in.readTypedList(getDay(), HalfDayForecast.CREATOR);
        }
        if(in.readInt() == 1) {
            setDayMaxTemp(in.readString());
        }
        if(in.readInt() == 1) {
            setNight(new ArrayList<HalfDayForecast>());
            in.readTypedList(getNight(), HalfDayForecast.CREATOR);
        }
        if(in.readInt() == 1) {
            setNightMinTemp(in.readString());
        }
        if(in.readInt() == 1) {
            setTempUnit(in.readString());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(!TextUtils.isEmpty(getDate()) ? 1 : 0);
        if(!TextUtils.isEmpty(getDate())) {
            dest.writeString(getDate());
        }
        dest.writeInt(getDay() != null ? 1 : 0);
        if(getDay() != null) {
            dest.writeTypedList(getDay());
        }
        dest.writeInt(!TextUtils.isEmpty(getDayMaxTemp()) ? 1 : 0);
        if(!TextUtils.isEmpty(getDayMaxTemp())) {
            dest.writeString(getDayMaxTemp());
        }
        dest.writeInt(getNight() != null ? 1 : 0);
        if(getNight() != null) {
            dest.writeTypedList(getNight());
        }
        dest.writeInt(!TextUtils.isEmpty(getNightMinTemp()) ? 1 : 0);
        if(!TextUtils.isEmpty(getNightMinTemp())) {
            dest.writeString(getNightMinTemp());
        }
        dest.writeInt(!TextUtils.isEmpty(getTempUnit()) ? 1 : 0);
        if(!TextUtils.isEmpty(getTempUnit())) {
            dest.writeString(getTempUnit());
        }
    }

    public static final Parcelable.Creator<Forecast> CREATOR = new Creator<Forecast>() {
        @Override
        public Forecast createFromParcel(Parcel parcel) {
            return new Forecast(parcel);
        }

        @Override
        public Forecast[] newArray(int size) {
            return new Forecast[size];
        }
    };

    public String getDate() {
        return date;
    }

    /**
     * Get the date for this forecast as a Calendar object. If there is a problem in parsing the date string it will return Today.
     * @return The date parsed from the date String or Today if there are issues
     */
    public Calendar getDateAsCal() {
        Calendar result = Calendar.getInstance();

        if(!TextUtils.isEmpty(date)) {
            try {
                result.setTime(new SimpleDateFormat(DATE_FORMAT, Locale.US).parse(date));
            } catch(ParseException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public List<HalfDayForecast> getDay() {
        return day;
    }

    public void setDay(List<HalfDayForecast> day) {
        this.day = day;
    }

    public String getDayMaxTemp() {
        return dayMaxTemp;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getDayMaxTempAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(dayMaxTemp)) {
            try {
                result = Integer.valueOf(dayMaxTemp);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setDayMaxTemp(String dayMaxTemp) {
        this.dayMaxTemp = dayMaxTemp;
    }

    public List<HalfDayForecast> getNight() {
        return night;
    }

    public void setNight(List<HalfDayForecast> night) {
        this.night = night;
    }

    public String getNightMinTemp() {
        return nightMinTemp;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getNightMinTempAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(nightMinTemp)) {
            try {
                result = Integer.valueOf(nightMinTemp);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setNightMinTemp(String nightMinTemp) {
        this.nightMinTemp = nightMinTemp;
    }

    public String getTempUnit() {
        return tempUnit;
    }

    public void setTempUnit(String tempUnit) {
        this.tempUnit = tempUnit;
    }

    @Override
    public String toString() {
        return GsonProvider.getInstance().getGson().toJson(this);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(!TextUtils.isEmpty(date)) {
            hash = hash * 5 + date.hashCode();
        }
        if(day != null) {
            hash = hash * 17 + day.hashCode();
        }
        if(!TextUtils.isEmpty(dayMaxTemp)) {
            hash = hash * 7 + dayMaxTemp.hashCode();
        }
        if(night != null) {
            hash = hash * 4 + night.hashCode();
        }
        if(!TextUtils.isEmpty(nightMinTemp)) {
            hash = hash * 6 + nightMinTemp.hashCode();
        }
        if(!TextUtils.isEmpty(tempUnit)) {
            hash = hash * 14 + tempUnit.hashCode();
        }

        return hash;
    }

    @Override
    public boolean equals(Object in) {
        boolean result = true;

        if(!(in instanceof Forecast)) {
            result = false;
        }

        if(result) {
            Forecast cast = (Forecast) in;

            if(TextUtils.isEmpty(date)) {
                if(!TextUtils.isEmpty(cast.date)) {
                    result = false;
                }
            } else if(!date.equals(cast.date)) {
                result = false;
            }

            if(day == null) {
                if(cast.day != null) {
                    result = false;
                }
            } else if(cast.day == null) {
                result = false;
            } else if(day.size() != cast.day.size()) {
                result = false;
            } else {
                for(int i = 0; i < day.size(); i++) {
                    if(!day.get(i).equals(cast.day.get(i))) {
                        result = false;
                    }
                }
            }

            if(TextUtils.isEmpty(dayMaxTemp)) {
                if(!TextUtils.isEmpty(cast.dayMaxTemp)) {
                    result = false;
                }
            } else if(!dayMaxTemp.equals(cast.dayMaxTemp)) {
                result = false;
            }

            if(night == null) {
                if(cast.night != null) {
                    result = false;
                }
            } else if(cast.day == null) {
                result = false;
            } else if(night.size() != cast.night.size()) {
                result = false;
            } else {
                for(int i = 0; i < night.size(); i++) {
                    if(!night.get(i).equals(cast.night.get(i))) {
                        result = false;
                    }
                }
            }

            if(TextUtils.isEmpty(nightMinTemp)) {
                if(!TextUtils.isEmpty(cast.nightMinTemp)) {
                    result = false;
                }
            } else if(!nightMinTemp.equals(cast.nightMinTemp)) {
                result = false;
            }

            if(TextUtils.isEmpty(tempUnit)) {
                if(!TextUtils.isEmpty(cast.tempUnit)) {
                    result = false;
                }
            } else if(!tempUnit.equals(cast.tempUnit)) {
                result = false;
            }
        }


        return result;
    }
}
