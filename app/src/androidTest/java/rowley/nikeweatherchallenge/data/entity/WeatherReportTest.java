package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.text.TextUtils;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class WeatherReportTest extends WeatherEntityTestParent {

    public void testWeatherReportParcel() {
        Parcel parcel = Parcel.obtain();
        report.writeToParcel(parcel, 0);
        parcel.setDataPosition(0);

        WeatherReport newReport = WeatherReport.CREATOR.createFromParcel(parcel);

        assertNotNull(newReport);
        assertTrue(report.equals(newReport));
    }

    public void testWeatherReportSerialize() {
        String json = GsonProvider.getInstance().getGson().toJson(report);

        assertFalse(TextUtils.isEmpty(json));
        assertEquals(reportJson, json);
    }
}
