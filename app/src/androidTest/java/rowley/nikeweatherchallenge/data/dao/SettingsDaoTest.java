package rowley.nikeweatherchallenge.data.dao;

import rowley.nikeweatherchallenge.WeatherChallengeActivityTestBase;

/**
 * Created by joe on 4/5/15.
 */
public class SettingsDaoTest extends WeatherChallengeActivityTestBase {
    private SettingsDao mDao;

    //to clean up
    private Boolean originalIsMetric = null;
    private Long originalLastUpdated = null;
    private Integer originalZipCode = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mDao = new SettingsDao(mActivity);
    }

    @Override
    public void tearDown() throws Exception {
        if(originalIsMetric != null) {
            mDao.setIsMetric(originalIsMetric);
        }
        if(originalLastUpdated != null) {
            mDao.setReportLastUpdated(originalLastUpdated);
        }
        if(originalZipCode != null) {
            mDao.setZipCode(originalZipCode);
        }

        super.tearDown();
    }

    public void testSetUpdateIsMetric() {
        originalIsMetric = mDao.getIsMetric();
        assertNotNull(originalIsMetric);

        assertTrue("The set did not succeed", mDao.setIsMetric(!originalIsMetric));
        assertEquals("The value did not update", !originalIsMetric, mDao.getIsMetric());
    }

    public void testSetUpdateLastUpdated() {
        originalLastUpdated = mDao.getReportLastUpdated();
        assertNotNull(originalLastUpdated);

        long updateValue = System.currentTimeMillis();
        assertTrue("The set did not succeed", mDao.setReportLastUpdated(updateValue));
        assertEquals("The value did not update", updateValue, mDao.getReportLastUpdated());
    }

    public void testSetUpdateZipCode() {
        originalZipCode = mDao.getZipCode();
        assertNotNull(originalZipCode);

        int newZip = 83687;
        assertTrue("The update didn't succeed", mDao.setZipCode(newZip));
        assertEquals("the value did not update", newZip, mDao.getZipCode());
    }
}
