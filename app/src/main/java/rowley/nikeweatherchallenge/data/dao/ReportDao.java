package rowley.nikeweatherchallenge.data.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.CursorIndexOutOfBoundsException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteStatement;

import com.google.gson.JsonParseException;
import com.google.gson.stream.MalformedJsonException;

import java.util.Locale;

import rowley.nikeweatherchallenge.data.WeatherDbHelper;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/5/15.
 */
public class ReportDao extends WeatherDbHelper {
    public static final String TABLE_NAME = "weather_report";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_TIME_LOADED = "time_loaded";
    public static final String COLUMN_JSON = "json";

    public ReportDao(Context context) {
        super(context);
    }

    public boolean saveReport(long retrievedTimeInMillis, WeatherReport report) {
        boolean result = false;

        if(report != null) {
            SQLiteDatabase db = getWritableDatabase();
            //first, check to see if we already have a record
            long id = -1;
            Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_ID}, String.format(Locale.US, "%s IS NOT NULL", COLUMN_TIME_LOADED),
                    null, null, null, null);
            if (cursor != null) {
                try {
                    cursor.moveToFirst();
                    id = cursor.getLong(0);
                } catch (SQLiteException e) {
                    e.printStackTrace();
                } catch (CursorIndexOutOfBoundsException e) {
                    e.printStackTrace();
                } finally {
                    cursor.close();
                }
            }

            db.beginTransaction();

            SQLiteStatement stmt = null;
            try {
                if (id >= 0) {
                    stmt = db.compileStatement(String.format(Locale.US, "UPDATE %s SET %s=?, %s=? WHERE %s=?", TABLE_NAME, COLUMN_TIME_LOADED, COLUMN_JSON, COLUMN_ID));
                    stmt.bindLong(1, retrievedTimeInMillis);
                    stmt.bindString(2, GsonProvider.getInstance().getGson().toJson(report));
                    stmt.bindLong(3, id);

                    stmt.executeUpdateDelete();
                    db.setTransactionSuccessful();
                    result = true;
                } else {
                    stmt = db.compileStatement(String.format(Locale.US, "INSERT INTO %s (%s, %s) VALUES (?, ?)", TABLE_NAME, COLUMN_TIME_LOADED, COLUMN_JSON));
                    stmt.bindLong(1, retrievedTimeInMillis);
                    stmt.bindString(2, GsonProvider.getInstance().getGson().toJson(report));

                    stmt.executeInsert();
                    db.setTransactionSuccessful();
                    result = true;
                }
            } catch (SQLiteException e) {
                e.printStackTrace();
            } finally {
                if (stmt != null) {
                    stmt.close();
                    db.endTransaction();
                }
            }

            db.close();
        }

        return result;
    }

    public WeatherReport getLatestWeatherReport() {
        WeatherReport result = null;

        SQLiteDatabase db = getWritableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[]{COLUMN_JSON}, String.format(Locale.US, "%s IS NOT NULL", COLUMN_TIME_LOADED),
                null, null, null, String.format(Locale.US, "%s DESC", COLUMN_TIME_LOADED));
        if(cursor != null) {
            try {
                cursor.moveToFirst();
                result = GsonProvider.getInstance().getGson().fromJson(cursor.getString(0), WeatherReport.class);
            } catch (SQLiteException e) {
                e.printStackTrace();
            } catch (CursorIndexOutOfBoundsException e) {
                e.printStackTrace();
            } catch (JsonParseException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
            }
        }

        db.close();

        return result;
    }
}
