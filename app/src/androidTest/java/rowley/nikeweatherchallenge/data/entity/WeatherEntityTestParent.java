package rowley.nikeweatherchallenge.data.entity;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.util.GsonProvider;
import rowley.nikeweatherchallenge.WeatherChallengeActivityTestBase;

/**
 * Created by joe on 4/4/15.
 */
public abstract class WeatherEntityTestParent extends WeatherChallengeActivityTestBase {
    protected WeatherReport report;
    protected String reportJson;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        try {
            InputStream input = mActivity.getResources().openRawResource(R.raw.weather_report);
            ByteArrayOutputStream byteOutput = new ByteArrayOutputStream();
            byte[] part = new byte[2048];
            int numRead;
            while ((numRead = input.read(part)) != -1) {
                byteOutput.write(part, 0, numRead);
            }
            input.close();

            reportJson = byteOutput.toString();
            report = GsonProvider.getInstance().getGson().fromJson(reportJson, WeatherReport.class);
        } catch(IOException e) {
            e.printStackTrace();
        }

        assertNotNull(report);
    }
}
