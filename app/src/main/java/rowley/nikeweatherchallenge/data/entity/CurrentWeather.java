package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class CurrentWeather implements Serializable, Parcelable {
    private String humidity;
    private String pressure;
    private String temp;
    @SerializedName("temp_unit")
    private String tempUnit;
    @SerializedName("weather_code")
    private String weatherCode;
    @SerializedName("weather_text")
    private String weatherText;
    private List<Wind> wind;

    public CurrentWeather() {}

    private CurrentWeather(Parcel in) {
        if(in.readInt() == 1) {
            setHumidity(in.readString());
        }
        if(in.readInt() == 1) {
            setPressure(in.readString());
        }
        if(in.readInt() == 1) {
            setTemp(in.readString());
        }
        if(in.readInt() == 1) {
            setTempUnit(in.readString());
        }
        if(in.readInt() == 1) {
            setWeatherCode(in.readString());
        }
        if(in.readInt() == 1) {
            setWeatherText(in.readString());
        }
        if(in.readInt() == 1) {
            setWind(new ArrayList<Wind>());
            in.readTypedList(getWind(), Wind.CREATOR);
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(!TextUtils.isEmpty(getHumidity()) ? 1 : 0);
        if(!TextUtils.isEmpty(getHumidity())) {
            dest.writeString(getHumidity());
        }
        dest.writeInt(!TextUtils.isEmpty(getPressure()) ? 1 : 0);
        if(!TextUtils.isEmpty(getPressure())) {
            dest.writeString(getPressure());
        }
        dest.writeInt(!TextUtils.isEmpty(getTemp()) ? 1 : 0);
        if(!TextUtils.isEmpty(getTemp())) {
            dest.writeString(getTemp());
        }
        dest.writeInt(!TextUtils.isEmpty(getTempUnit()) ? 1 : 0);
        if(!TextUtils.isEmpty(getTempUnit())) {
            dest.writeString(getTempUnit());
        }
        dest.writeInt(!TextUtils.isEmpty(getWeatherCode()) ? 1 : 0);
        if(!TextUtils.isEmpty(getWeatherCode())) {
            dest.writeString(getWeatherCode());
        }
        dest.writeInt(!TextUtils.isEmpty(getWeatherText()) ? 1 : 0);
        if(!TextUtils.isEmpty(getWeatherText())) {
            dest.writeString(getWeatherText());
        }
        dest.writeInt(getWind() != null ? 1 : 0);
        if(getWind() != null) {
            dest.writeTypedList(getWind());
        }
    }

    public static final Parcelable.Creator<CurrentWeather> CREATOR = new Creator<CurrentWeather>() {
        @Override
        public CurrentWeather createFromParcel(Parcel parcel) {
            return new CurrentWeather(parcel);
        }

        @Override
        public CurrentWeather[] newArray(int size) {
            return new CurrentWeather[size];
        }
    };

    public String getHumidity() {
        return humidity;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getHumidityAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(humidity)) {
            try {
                result = Integer.valueOf(humidity);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getPressureAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(pressure)) {
            try {
                result = Integer.valueOf(pressure);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public String getTemp() {
        return temp;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getTempAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(temp)) {
            try {
                result = Integer.valueOf(temp);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setTemp(String temp) {
        this.temp = temp;
    }

    public String getTempUnit() {
        return tempUnit;
    }

    public void setTempUnit(String tempUnit) {
        this.tempUnit = tempUnit;
    }

    public String getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(String weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getWeatherText() {
        return weatherText;
    }

    public void setWeatherText(String weatherText) {
        this.weatherText = weatherText;
    }

    public List<Wind> getWind() {
        return wind;
    }

    public void setWind(List<Wind> wind) {
        this.wind = wind;
    }

    @Override
    public String toString() {
        return GsonProvider.getInstance().getGson().toJson(this);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(!TextUtils.isEmpty(humidity)) {
            hash = hash * 5 + humidity.hashCode();
        }
        if(!TextUtils.isEmpty(pressure)) {
            hash = hash * 17 + pressure.hashCode();
        }
        if(!TextUtils.isEmpty(temp)) {
            hash = hash * 7 + temp.hashCode();
        }
        if(!TextUtils.isEmpty(tempUnit)) {
            hash = hash * 14 + tempUnit.hashCode();
        }
        if(!TextUtils.isEmpty(weatherCode)) {
            hash = hash * 4 + weatherCode.hashCode();
        }
        if(!TextUtils.isEmpty(weatherText)) {
            hash = hash * 6 + weatherText.hashCode();
        }
        if(wind != null && !wind.isEmpty()) {
            for (Wind windObj : wind) {
                hash = hash * 21 + windObj.hashCode();
            }
        }

        return hash;
    }

    @Override
    public boolean equals(Object in) {
        boolean result = true;

        if(!(in instanceof CurrentWeather)) {
            result = false;
        }

        if(result) {
            CurrentWeather cast = (CurrentWeather) in;

            if(TextUtils.isEmpty(humidity)) {
                if(!TextUtils.isEmpty(cast.humidity)) {
                    result = false;
                }
            } else if(!humidity.equals(cast.humidity)) {
                result = false;
            }

            if(TextUtils.isEmpty(pressure)) {
                if(!TextUtils.isEmpty(cast.pressure)) {
                    result = false;
                }
            } else if(!pressure.equals(cast.pressure)) {
                result = false;
            }

            if(TextUtils.isEmpty(temp)) {
                if(!TextUtils.isEmpty(cast.temp)) {
                    result = false;
                }
            } else if(!temp.equals(cast.temp)) {
                result = false;
            }

            if(TextUtils.isEmpty(tempUnit)) {
                if(!TextUtils.isEmpty(cast.tempUnit)) {
                    result = false;
                }
            } else if(!tempUnit.equals(cast.tempUnit)) {
                result = false;
            }

            if(TextUtils.isEmpty(weatherCode)) {
                if(!TextUtils.isEmpty(cast.weatherCode)) {
                    result = false;
                }
            } else if(!weatherCode.equals(cast.weatherCode)) {
                result = false;
            }

            if(TextUtils.isEmpty(weatherText)) {
                if(!TextUtils.isEmpty(cast.weatherText)) {
                    result = false;
                }
            } else if(!weatherText.equals(cast.weatherText)) {
                result = false;
            }

            if(wind == null) {
                if(cast.wind != null) {
                    result = false;
                }
            } else if(cast.wind == null) {
                result = false;
            } else if(wind.size() != cast.wind.size()) {
                result = false;
            } else {
                for(int i = 0; i < wind.size(); i++) {
                    if(!wind.get(i).equals(cast.wind.get(i))) {
                        result = false;
                    }
                }
            }
        }


        return result;
    }
}
