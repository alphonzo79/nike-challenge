package rowley.nikeweatherchallenge.data.entity;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import rowley.nikeweatherchallenge.util.GsonProvider;

/**
 * Created by joe on 4/4/15.
 */
public class Wind implements Serializable, Parcelable {
    private String dir;
    @SerializedName("dir_degree")
    private String dirDegrees;
    private String speed;
    @SerializedName("wind_unit")
    private String windUnit;

    public Wind() {}

    private Wind(Parcel in) {
        if(in.readInt() == 1) {
            setDir(in.readString());
        }
        if(in.readInt() == 1) {
            setDirDegrees(in.readString());
        }
        if(in.readInt() == 1) {
            setSpeed(in.readString());
        }
        if(in.readInt() == 1) {
            setWindUnit(in.readString());
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(!TextUtils.isEmpty(getDir()) ? 1 : 0);
        if(!TextUtils.isEmpty(getDir())) {
            dest.writeString(getDir());
        }
        dest.writeInt(!TextUtils.isEmpty(getDirDegrees()) ? 1 : 0);
        if(!TextUtils.isEmpty(getDirDegrees())) {
            dest.writeString(getDirDegrees());
        }
        dest.writeInt(!TextUtils.isEmpty(getSpeed()) ? 1 : 0);
        if(!TextUtils.isEmpty(getSpeed())) {
            dest.writeString(getSpeed());
        }
        dest.writeInt(!TextUtils.isEmpty(getWindUnit()) ? 1 : 0);
        if(!TextUtils.isEmpty(getWindUnit())) {
            dest.writeString(getWindUnit());
        }
    }

    public static final Parcelable.Creator<Wind> CREATOR = new Creator<Wind>() {
        @Override
        public Wind createFromParcel(Parcel parcel) {
            return new Wind(parcel);
        }

        @Override
        public Wind[] newArray(int size) {
            return new Wind[size];
        }
    };

    public String getDir() {
        return dir;
    }

    public void setDir(String dir) {
        this.dir = dir;
    }

    public String getDirDegrees() {
        return dirDegrees;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getDirDegreesAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(dirDegrees)) {
            try {
                result = Integer.valueOf(dirDegrees);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        } else if(!TextUtils.isEmpty(dir)) {
            switch(dir) {
                case "N":
                    result = 0;
                    break;
                case "NNE":
                    result = 22;
                    break;
                case "NE":
                    result = 45;
                    break;
                case "ENE":
                    result = 67;
                    break;
                case "E":
                    result = 90;
                    break;
                case "ESE":
                    result = 112;
                    break;
                case "SE":
                    result = 135;
                    break;
                case "SSE":
                    result = 157;
                    break;
                case "S":
                    result = 180;
                    break;
                case "SSW":
                    result = 202;
                    break;
                case "SW":
                    result = 225;
                    break;
                case "WSW":
                    result = 247;
                    break;
                case "W":
                    result = 270;
                    break;
                case "WNW":
                    result = 292;
                    break;
                case "NW":
                    result = 315;
                    break;
                case "NNW":
                    result = 337;
                    break;
                default:
                    result = 0;
                    break;
            }
        }

        return result;
    }

    public void setDirDegrees(String dirDegrees) {
        this.dirDegrees = dirDegrees;
    }

    public String getSpeed() {
        return speed;
    }

    /**
     * Convert the string to an int value.
     * @return The int value of the string or {@link Integer#MIN_VALUE} if the string is null or empty or if there is a NumberFormatException
     */
    public int getSpeedAsInt() {
        int result = Integer.MIN_VALUE;

        if(!TextUtils.isEmpty(speed)) {
            try {
                result = Integer.valueOf(speed);
            } catch(NumberFormatException e) {
                e.printStackTrace();
            }
        }

        return result;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getWindUnit() {
        return windUnit;
    }

    public void setWindUnit(String windUnit) {
        this.windUnit = windUnit;
    }

    @Override
    public String toString() {
        return GsonProvider.getInstance().getGson().toJson(this);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        if(!TextUtils.isEmpty(dir)) {
            hash = hash * 5 + dir.hashCode();
        }
        if(!TextUtils.isEmpty(dirDegrees)) {
            hash = hash * 17 + dirDegrees.hashCode();
        }
        if(!TextUtils.isEmpty(speed)) {
            hash = hash * 7 + speed.hashCode();
        }
        if(!TextUtils.isEmpty(windUnit)) {
            hash = hash * 14 + windUnit.hashCode();
        }

        return hash;
    }

    @Override
    public boolean equals(Object in) {
        boolean result = true;

        if(!(in instanceof Wind)) {
            result = false;
        }

        if(result) {
            Wind cast = (Wind) in;

            if(TextUtils.isEmpty(dir)) {
                if(!TextUtils.isEmpty(cast.dir)) {
                    result = false;
                }
            } else if(!dir.equals(cast.dir)) {
                result = false;
            }

            if(TextUtils.isEmpty(dirDegrees)) {
                if(!TextUtils.isEmpty(cast.dirDegrees)) {
                    result = false;
                }
            } else if(!dirDegrees.equals(cast.dirDegrees)) {
                result = false;
            }

            if(TextUtils.isEmpty(speed)) {
                if(!TextUtils.isEmpty(cast.speed)) {
                    result = false;
                }
            } else if(!speed.equals(cast.speed)) {
                result = false;
            }

            if(TextUtils.isEmpty(windUnit)) {
                if(!TextUtils.isEmpty(cast.windUnit)) {
                    result = false;
                }
            } else if(!windUnit.equals(cast.windUnit)) {
                result = false;
            }
        }


        return result;
    }
}
