package rowley.nikeweatherchallenge.interfaces;

import rowley.nikeweatherchallenge.data.entity.WeatherReport;

/**
 * Created by joe on 4/5/15.
 */
public interface OnReceiveWeatherReportListener {
    public void onReceiveWeatherReport(WeatherReport report, boolean isMetric, int zipCode);
}
