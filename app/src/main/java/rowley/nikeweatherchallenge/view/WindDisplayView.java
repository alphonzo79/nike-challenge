package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.Locale;

import rowley.nikeweatherchallenge.R;
import rowley.nikeweatherchallenge.data.entity.Wind;
import rowley.nikeweatherchallenge.util.MetricConverter;

/**
 * Created by joe on 4/8/15.
 */
public class WindDisplayView extends RelativeLayout {
    private ImageView arrow;
    private TextView speed;

    public WindDisplayView(Context context) {
        super(context);
        initView(context);
    }

    public WindDisplayView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public WindDisplayView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView(context);
    }

    private void initView(Context context) {
        inflate(context, R.layout.view_wind_display, this);

        arrow = (ImageView)findViewById(R.id.wind_arrow);
        speed = (TextView)findViewById(R.id.wind_speed);
    }

    public void setData(Wind windData, boolean isMetric) {
        if(windData != null && windData.getDirDegreesAsInt() != Integer.MIN_VALUE) {
            arrow.setRotation(windData.getDirDegreesAsInt());
            arrow.setVisibility(VISIBLE);
        } else {
            arrow.setVisibility(GONE);
        }

        String speedString = "";
        if(windData != null && windData.getSpeedAsInt() != Integer.MIN_VALUE) {
            int windSpeed = windData.getSpeedAsInt();
            String windUnits = "";
            if(isMetric) {
                windUnits = getResources().getString(R.string.kph_unit_backup);
                windSpeed = MetricConverter.getWindSpeedInMetric(windSpeed, windData.getWindUnit());
            } else {
                windUnits = getResources().getString(R.string.mph_unit_backup);
                windSpeed = MetricConverter.getWindSpeedInImperial(windSpeed, windData.getWindUnit());
            }
            speedString = String.format(Locale.US, getResources().getString(R.string.wind_speed_formatted), windSpeed, windUnits);

            if(windSpeed == 0) {
                arrow.setVisibility(GONE);
            } else {
                arrow.setVisibility(VISIBLE);
            }
        }
        speed.setText(speedString);
    }
}
