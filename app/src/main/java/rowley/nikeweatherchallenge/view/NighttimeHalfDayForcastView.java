package rowley.nikeweatherchallenge.view;

import android.content.Context;
import android.util.AttributeSet;

import rowley.nikeweatherchallenge.R;

/**
 * Created by joe on 4/8/15.
 */
public class NighttimeHalfDayForcastView extends HalfDayForcastViewParent {
    public NighttimeHalfDayForcastView(Context context) {
        super(context);
    }

    public NighttimeHalfDayForcastView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NighttimeHalfDayForcastView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected int getBackgroundResource() {
        return R.color.night_back;
    }

    @Override
    protected String getTemperatureStringFormatted() {
        return getResources().getString(R.string.night_time_low_formatted);
    }
}
