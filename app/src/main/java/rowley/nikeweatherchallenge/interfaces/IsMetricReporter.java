package rowley.nikeweatherchallenge.interfaces;

/**
 * Created by joe on 4/5/15.
 */
public interface IsMetricReporter {
    public boolean getIsMetric();
}
