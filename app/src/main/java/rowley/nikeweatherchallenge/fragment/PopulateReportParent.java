package rowley.nikeweatherchallenge.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import rowley.nikeweatherchallenge.data.entity.CurrentWeather;
import rowley.nikeweatherchallenge.data.entity.Forecast;
import rowley.nikeweatherchallenge.data.entity.WeatherReport;
import rowley.nikeweatherchallenge.interfaces.OnReceiveWeatherReportListener;
import rowley.nikeweatherchallenge.interfaces.ReportReceiverRegistrationListener;
import rowley.nikeweatherchallenge.view.CurrentDisplayView;
import rowley.nikeweatherchallenge.view.TomorrowDisplayView;

/**
 * Created by joe on 4/9/15.
 */
public abstract class PopulateReportParent extends Fragment implements OnReceiveWeatherReportListener {
    protected WeatherReport report;
    protected boolean isMetric;
    protected int zipCode;

    protected CurrentDisplayView currentDisplayView;
    protected TomorrowDisplayView tomorrowDisplayView;

    protected String todayDateString;
    protected String tomorrowDateString;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if(activity instanceof ReportReceiverRegistrationListener) {
            ((ReportReceiverRegistrationListener)activity).registerReportReceiver(this);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat(Forecast.DATE_FORMAT, Locale.US);
        todayDateString = sdf.format(cal.getTime());
        cal.roll(Calendar.DAY_OF_MONTH, true);
        tomorrowDateString = sdf.format(cal.getTime());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if(getActivity() instanceof ReportReceiverRegistrationListener) {
            ((ReportReceiverRegistrationListener)getActivity()).removeReportReceiver(this);
        }
    }

    @Override
    public void onReceiveWeatherReport(WeatherReport report, boolean isMetric, int zipCode) {
        this.report = report;
        this.isMetric = isMetric;
        this.zipCode = zipCode;

        populateRelevantViews();
    }

    protected void setDataToCurrentDisplayView() {
        if(currentDisplayView != null) {
            CurrentWeather currentWeather = null;
            if(report != null && report.getCurrentWeather() != null && !report.getCurrentWeather().isEmpty()) {
                currentWeather = report.getCurrentWeather().get(0);
            }
            Forecast forecast = null;
            if(report != null && report.getForecast() != null && !report.getForecast().isEmpty()) {
                for(Forecast fc : report.getForecast()) {
                    if(!TextUtils.isEmpty(fc.getDate()) && fc.getDate().equals(todayDateString)) {
                        forecast = fc;
                        break;
                    }
                }
            }
            currentDisplayView.setCurrentWeather(currentWeather, forecast, isMetric, zipCode);
        }
    }

    protected void setDataToTomorrowDisplayView() {
        if(tomorrowDisplayView != null) {
            Forecast forecast = null;
            if(report != null && report.getForecast() != null && !report.getForecast().isEmpty()) {
                for(Forecast fc : report.getForecast()) {
                    if(!TextUtils.isEmpty(fc.getDate()) && fc.getDate().equals(tomorrowDateString)) {
                        forecast = fc;
                        break;
                    }
                }
            }
            tomorrowDisplayView.setForecast(forecast, isMetric, zipCode);
        }
    }

    protected abstract void populateRelevantViews();
}
