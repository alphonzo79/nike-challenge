package rowley.nikeweatherchallenge;

import android.test.ActivityInstrumentationTestCase2;

import rowley.nikeweatherchallenge.activity.TestStubActivity;

/**
 * Created by joe on 4/4/15.
 */
public abstract class WeatherChallengeActivityTestBase extends ActivityInstrumentationTestCase2<TestStubActivity> {
    protected TestStubActivity mActivity;

    public WeatherChallengeActivityTestBase(Class<TestStubActivity> activityClass) {
        super(activityClass);
    }

    public WeatherChallengeActivityTestBase() {
        this(TestStubActivity.class);
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();
        mActivity = getActivity();
    }
}
